const express = require("express");
const Product = require('../models/product');
const router = express.Router();

router.get("", (req, res, next) => {
    const pageSize = +req.query.pagesize;//convert as numeric
    const currentPage = +req.query.page;
    const postQuery = Product.find();
    let fetchedPosts;
    if (pageSize && currentPage){
        postQuery
            .skip(pageSize * (currentPage - 1))
            .limit(pageSize);
    }
    postQuery
        .then(documents => {
            //console.log(documents);
            fetchedPosts = documents;
            return Product.countDocuments();
         })
         .then(count => {
            //console.log(count);
             res.status(200).json({
                 message: "Posts fetched successfully!",
                 posts: fetchedPosts,
                 maxPosts: count
             });
         })
 });

 router.get("/:id", (req, res, next) => {
    //console.log(req.params.id);
    Product.findById(req.params.id).then(product => {
        //console.log(product);
        if (product) {
            res.status(202).json(product);
        } else {
            res.status(404).json({message: 'Product not found'});
        }
    });
});


 module.exports = router;