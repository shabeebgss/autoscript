const express = require("express");
const StaticInvoiceShedule = require('../models/static-invoice-schedule');
const router = express.Router();

router.post("", (req, res, next) => {
    var randomToken = require('random-token').create('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    var token = randomToken(11);
    const staticInvoiceShedule = new StaticInvoiceShedule({
        immediate_cost: req.body.immediate_cost,
        service_name: req.body.service_name,
        amount: req.body.amount,
        frequency: req.body.frequency,
        schedule_id:token,
        payment_day: req.body.payment_day,
        payment_month: req.body.payment_month,
        schedule_type: req.body.schedule_type,
        qr_code: token,
        // creator: req.body.user_id,

    });
    staticInvoiceShedule.save()
    .then(createdShedule => {
        console.log(createdShedule);
     res.status(201).json({
         message: 'Shedule added successfully',
         sheduleId: createdShedule.schedule_id,
         shedulePadentId: createdShedule._id,
         qr_code:createdShedule.qr_code
     });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
 });

router.get("", (req, res, next) => {
    
     Post.find('5bd83349e9c65e084463c2b1').then(sheduleDetails => {
        console.log(sheduleDetails);
         if (post) {
             res.status(202).json(post);
         } else {
             res.status(404).json({message: 'Post not found'});
         }
     })
     .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
 });
 
//  router.put("/:id", (req, res, next) => {
//      const post = new Post({
//          _id: req.body.id,
//          title: req.body.title,
//          content: req.body.content
//      });
//      Post.updateOne({ _id: req.params.id },post).then(result => {
//          res.status(200).json({ message: 'Update successful!' })
//          });       
//  });
 
//  router.get("", (req, res, next) => {
//     const pageSize = +req.query.pagesize;//convert as numeric
//     const currentPage = +req.query.page;
//     const postQuery = Post.find();
//     let fetchedPosts;
//     if (pageSize && currentPage){
//         postQuery
//             .skip(pageSize * (currentPage - 1))
//             .limit(pageSize);
//     }
//     postQuery
//         .then(documents => {
//             fetchedPosts = documents;
//             return Post.count();
//          })
//          .then(count => {
//              res.status(200).json({
//                  message: "Posts fetched successfully!",
//                  posts: fetchedPosts,
//                  maxPosts: count
//              });
//          })
//  });
 

 
//  router.delete("/:id", (req, res, next) => {
//      Post.deleteOne({ _id: req.params.id}).then(result => {
//          console.log(result);
//          res.status(200).json({ message: "Post deleted!"});
//      });
//  });

 module.exports = router;