const express = require("express");
const axios = require('axios');
const Invoice = require('../models/invoice');
const router = express.Router();
const schedule = require('node-schedule');



router.post("", (req, res, next) => {
    var randomToken = require('random-token').create('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    var token = randomToken(11);
    if(req.body.immediatePaymentAmount) {
        var payment_amount = req.body.immediatePaymentAmount;
    }
    const invoice = new Invoice({
        payment_day: req.body.paymentDay,
        immediate_cost: req.body.immediatePaymentAmount,
        regular_amount: req.body.paymentAmount,
        payment_amount: payment_amount,
        schedule_type: req.body.schedule_type,
        payment_status:1,
        paid_on:'',
        payment_month: req.body.payment_month,
        payment_date: req.body.payment_date,
        qr_code: token,
        schedule_parent_id: req.body.shedule_parent_id,
        schedule_id: token,
    });
    invoice.save()
    .then(Invoice => {
     res.status(201).json({
         message: 'Invoice added successfully',
         id: Invoice._id,
         qr_code: Invoice.qr_code,
         payment_status: Invoice.payment_status
     });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
 });


 router.get("/:id", (req, res, next) => {
    var j = schedule.scheduleJob('3 * * * * *', function () {
    Invoice.findById(req.params.id).then(invoice => {
        console.log("from first job");
        if (invoice) {
            res.status(202).json(invoice);
        } else {
            res.status(404).json({message: 'Invoice not found'});
        }
        console.log(invoice.payment_status);
    });
});
});

// router.get("/:status", (req, res, next) => {

//     var j = schedule.scheduleJob('3 * * * * *', function () {
//         //console.log(req.params.id);
//     Invoice.findById(req.params.id).then(invoice => {
//         console.log("from cron job");
//         if (invoice) {
//             res.status(202).json(invoice);
//         } else {
//             res.status(404).json({message: 'Invoice not found'});
//         }
//         console.log(invoice.payment_status);
//     });
       
//     });
// });

// router.get("", (req, res, next) => {
//     Invoice.then(invoice => {
//         if (invoice) {
//             res.status(202).json(invoice);
//         } else {
//             res.status(404).json({message: 'Invoice not found'});
//         }
//     });
// });
 
 
 
 


 module.exports = router;