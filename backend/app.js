const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const PostsRoute = require('./routes/posts');
const ProductRoute = require('./routes/product');
const SheduleRoute = require('./routes/static-invoice-schedule');
const InvoiceRoute = require('./routes/invoice');

const app = express();

mongoose.connect(
    //"mongodb://unni:Password1!@ds115653.mlab.com:15653/mean-stackdb",
    "mongodb+srv://mongoDB:mailChimp123!@cluster0-ld9o7.mongodb.net/node-angular",
     { useNewUrlParser: true}
     ).then(() => {
         console.log('connected to database')
     })
     .catch(() =>{
         console.log("Connection failed")
     });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: false } ));


app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
     "Access-Control-Allow-Headers",
     "Origin, X-Requested-With, Content-Type, Accept"
     );
    res.setHeader(
     "Access-Control-Allow-Methods",
     "GET, POST, PATCH, PUT, DELETE, OPTIONS"
     ); 
    next();
});

app.use("/api/posts", PostsRoute);
app.use("/api/product", ProductRoute);
app.use("/api/shedule", SheduleRoute);
app.use("/api/invoice", InvoiceRoute);


module.exports = app;