const mongoose = require('mongoose');
const productSchema = mongoose.Schema({
        name: { type: String, required: false},
        cost: { type: Number, required: false },
        frequency: { type: String, required: false },      
});
module.exports = mongoose.model('products', productSchema);