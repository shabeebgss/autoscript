import { Component, OnInit } from "@angular/core";
import { ProductService } from '../product.service';
import { Product } from '../product.model';
import { ActivatedRoute, ParamMap  } from '@angular/router';


@Component({
    selector: 'app-productC',
    templateUrl: './product-productC.component.html',
    styleUrls: ['./product-productC.component.css', '../product.component.css']
})

export class ProductCComponent implements OnInit{
    constructor(public productService: ProductService, public route: ActivatedRoute){}

    selectedDate: number;
    iPayCost: number;
    newDay:string;
    paymentDate:number
    payment_day:number;
    payment_month:number;
    invoiceType:string ='regular';
    name: string = "EXPLORER PACK";
    serviceName: string = "C";
    cost: number = 0.005;
    frequency: string = "monthly";
    sheduleType: number = 2;
    default_date: number;
    post: Product;
    private mode = 'create';
    private productId: string;
    selected:number;
    default_day:string;
    payment_date:string;

    dates = [
        {value: 1, viewValue: '01'},
        {value: 2, viewValue: '02'},
        {value: 3, viewValue: '03'},
        {value: 4, viewValue: '04'},
        {value: 5, viewValue: '05'},
        {value: 6, viewValue: '06'},
        {value: 7, viewValue: '07'},
        {value: 8, viewValue: '08'},
        {value: 9, viewValue: '09'},
        {value: 10, viewValue: '10'},
        {value: 11, viewValue: '11'},
        {value: 12, viewValue: '12'},
        {value: 13, viewValue: '13'},
        {value: 14, viewValue: '14'},
        {value: 15, viewValue: '15'},
        {value: 16, viewValue: '16'},
        {value: 17, viewValue: '17'},
        {value: 18, viewValue: '18'},
        {value: 19, viewValue: '19'},
        {value: 20, viewValue: '20'},
        {value: 21, viewValue: '21'},
        {value: 22, viewValue: '22'},
        {value: 23, viewValue: '23'},
        {value: 24, viewValue: '24'},
        {value: 25, viewValue: '25'},
        {value: 26, viewValue: '26'},
        {value: 27, viewValue: '27'},
        {value: 28, viewValue: '28'}
      ];
      
    

    
    ngOnInit(){
       
         /* set default date */
         const currDate = new Date();
       
         if ( currDate.getDate() > 28 ) {
             this.selected = 1;
         }
         else {
             this.selected = currDate.getDate() + 1;
         }

         var dd = currDate.getDate()+1;
         var mm = currDate.getMonth()+1; 
         var yyyy = currDate.getFullYear();
         if(dd > 28) {
             dd = 1;
         }
         this.paymentDate = dd;
         this.payment_day = dd;
         this.payment_month = mm;

         //selected date
       
       this.selectedDate = this.selected;
       this.payment_date = yyyy+'-'+this.payment_month+'-0'+this.payment_day;
 
    }    

    onSavePost() {

       
        this.productService.addShedule(this.iPayCost,
                                       this.serviceName,
                                       this.cost,
                                       2,
                                       this.payment_day,
                                       this.payment_month,
                                       this.payment_date,
                                       this.sheduleType,
                                       this.invoiceType
                                     );
    }

    
    

}



