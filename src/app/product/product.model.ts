
export interface Product {
    immediate_cost: number,
    service_name: string,
    amount: number,
    frequency:number,
    payment_day:number,
    payment_month:number,
    schedule_type:number,
}