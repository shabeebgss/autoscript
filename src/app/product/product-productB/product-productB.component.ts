import { Component, OnInit } from "@angular/core";
import { ProductService } from '../product.service';
import { Product } from '../product.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, ParamMap  } from '@angular/router';
import { RandomToken  } from 'random-token';

@Component({
    selector: 'app-productB',
    templateUrl: './product-productB.component.html',
    styleUrls: ['./product-productB.component.css']
})

export class ProductBComponent implements OnInit{
    constructor(public productService: ProductService, public route: ActivatedRoute){}

    selectedDate: number;
    iPayCost: number;
    newDay:string;
    payment_day:number;
    payment_month:number;
    paymentDate:number;
    invoiceType:string ='regular';
    post: Product;
    private mode = 'create';
    private productId: string;
    payment_date:string;

    name: string = "WAR EFFORT DONATION";
    serviceName: string = "B";
    cost: number = 0.003;
    frequency: string = "monthly";
    sheduleType: number = 2;
    default_date:number;
   
    
    ngOnInit(){
        this.route.paramMap.subscribe((paramMap: ParamMap) => {
             
            this.mode = 'create';
            this.productId = null;     
             
        });
        this.CalRegularPmt();
    }
    CalRegularPmt(){
        var currDate = new Date();
        var dd = currDate.getDate();
        var mm = currDate.getMonth()+1; 
        var yyyy = currDate.getFullYear();
        // this.paymentDate = dd+'-'+mm+'-'+yyyy;
        if(dd > 28) {
            dd = 1;
            mm = currDate.getMonth()+2;
        }
        this.paymentDate = dd;
        this.payment_day = dd;
        this.payment_month = mm;
        this.payment_date = yyyy+'-'+this.payment_month+'-0'+this.payment_day;

        

    }

    onSavePost(form:NgForm) {
        if (form.invalid){
            return;
        }
        // var randomToken = RandomToken.create('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        // var token = randomToken(11);
       
        this.productService.addShedule(this.iPayCost,
                                       this.serviceName,
                                       this.cost,
                                       2,
                                       this.payment_day,
                                       this.payment_month,
                                       this.payment_date,
                                       this.sheduleType,
                                       this.invoiceType
                                     );
    }
    
    

}



