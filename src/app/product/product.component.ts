import { Component, OnInit } from "@angular/core";
import { ProductService } from './product.service';
import { InvoiceService } from '../invoice/invoice.service';
import { Invoice } from '../invoice/invoice.model';
import { Product } from './product.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, ParamMap  } from '@angular/router';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit{
    constructor(public productService: ProductService, public invoiceService: InvoiceService, public route: ActivatedRoute){}

    payment_day:number;
    payment_month:number;
    product: Product;
    private mode = 'create';
    private productId: string;
    invoiceType:string = null;

    name: string = "Blast Off";
    serviceName: string = "A";
    selectedDate: number;
    cost: number = 0.002;
    iPayCost: number;
    frequency: string = "monthly";
    sheduleType: number = 2;
    newDay:string;
    payment_date:string;
    
    selected:number;
    default_day:string;
    
    
    

    ngOnInit() {

        this.route.paramMap.subscribe((paramMap: ParamMap) => {
             
            this.mode = 'create';
            this.productId = null;     
             
        });

        /* set default date */
        const currDate = new Date();
       
        if ( currDate.getDate() > 28 ) {
            this.selected = 1;
        }
        else {
            this.selected = currDate.getDate() + 2;
        }

       //selected date
       const dd = this.selected;
       this.selectedDate = this.selected;

       //current date
       var today = new Date();
       var today_dd = today.getDate();
       var today_mm = today.getMonth()+1; 
       var today_yyyy = today.getFullYear();
       var new_today = today_yyyy+'-'+today_mm+'-'+today_dd;

       if( dd >= today_dd) {
           var mm = currDate.getMonth()+1; 
       }  else {
           var mm = currDate.getMonth()+2; 
       }

       if( dd == today_dd) {
        this.invoiceType = 'regular';
       } else {
        this.invoiceType = 'immediate';
       }

       this.payment_day = dd;
       this.payment_month = mm;
       this.payment_date = today_yyyy+'-'+this.payment_month+'-0'+this.payment_day;
       const yyyy = currDate.getFullYear();
       const endDate = yyyy+'-'+mm+'-'+dd;
       
       /*compare the dates*/
       var d11 = new Date(endDate);
       var d22 = new Date(new_today);
       var timeDiff = d11.getTime() - d22.getTime();
       var days = timeDiff / (1000 * 3600 * 24);
       
       var per_remain_day = (days/30)*100;
      
           //this.iPayCost = this.cost / per_remain_day;
           this.iPayCost = this.cost / 30 * days;
 
    }

    dates = [
        {value: 1, viewValue: '01'},
        {value: 2, viewValue: '02'},
        {value: 3, viewValue: '03'},
        {value: 4, viewValue: '04'},
        {value: 5, viewValue: '05'},
        {value: 6, viewValue: '06'},
        {value: 7, viewValue: '07'},
        {value: 8, viewValue: '08'},
        {value: 9, viewValue: '09'},
        {value: 10, viewValue: '10'},
        {value: 11, viewValue: '11'},
        {value: 12, viewValue: '12'},
        {value: 13, viewValue: '13'},
        {value: 14, viewValue: '14'},
        {value: 15, viewValue: '15'},
        {value: 16, viewValue: '16'},
        {value: 17, viewValue: '17'},
        {value: 18, viewValue: '18'},
        {value: 19, viewValue: '19'},
        {value: 20, viewValue: '20'},
        {value: 21, viewValue: '21'},
        {value: 22, viewValue: '22'},
        {value: 23, viewValue: '23'},
        {value: 24, viewValue: '24'},
        {value: 25, viewValue: '25'},
        {value: 26, viewValue: '26'},
        {value: 27, viewValue: '27'},
        {value: 28, viewValue: '28'}
      ];
      
    
      
      selectChangeHandler(SelDate){
        this.selectedDate = SelDate;
        //this.iPayCost = +this.cost/+SelDate;  

        
        //selected date
        const currDate = new Date();
        const dd = this.selectedDate;
            
        //current date
        var today = new Date();
        var today_dd = today.getDate();
        var today_mm = today.getMonth()+1; 
        var today_yyyy = today.getFullYear();
        var new_today = today_yyyy+'-'+today_mm+'-'+today_dd;

        if( dd >= today_dd) {
            var mm = currDate.getMonth()+1; 
        }  else {
            var mm = currDate.getMonth()+2; 
        }
        this.payment_day = dd;
        this.payment_month = mm;
        const yyyy = currDate.getFullYear();
        const endDate = yyyy+'-'+mm+'-'+dd;

        /*compare the dates*/
        var d11 = new Date(endDate);
        var d22 = new Date(new_today);
        var timeDiff = d11.getTime() - d22.getTime();
        var days = timeDiff / (1000 * 3600 * 24);
            
        var per_remain_day = (days/30)*100;
           
        //this.iPayCost = this.cost / per_remain_day;
        this.iPayCost = this.cost / 30 * days;
        this.payment_date = today_yyyy+'-'+this.payment_month+'-'+this.payment_day;

    }
   
    onSavePost() {
        
        this.productService.addShedule(this.iPayCost,
                                       this.serviceName,
                                       this.cost,
                                        2,
                                        this.payment_day,
                                        this.payment_month,
                                        this.payment_date,
                                        this.sheduleType,
                                        this.invoiceType
                                     );
       // this.invoiceService.getShedule();                                     
        //form.resetForm();//clears field after submit
    }     
}



