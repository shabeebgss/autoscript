import { Component } from "@angular/core";

@Component({
    selector: 'app-productA',
    templateUrl: './product-productA.component.html',
    styleUrls: ['./product-productA.component.css']
})

export class ProductAComponent {
    product = {
        name: "Blast Off",
        immediatePaymentCost: "0.005",
        cost: "0.02",
        frequency: "monthly",
        paymentDate: "01"
    }

    dates = [
        {value: '0', viewValue: '1-28'},
        {value: '1', viewValue: '1-29'},
        {value: '2', viewValue: '1-30'}
      ];
}



