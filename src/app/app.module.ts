import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";



import { QRCodeModule } from 'angularx-qrcode';


import { 
  MatInputModule, 
  MatListModule, 
  MatCardModule,
  MatSelectModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatGridListModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
} from '@angular/material';

import { AppComponent } from './app.component';
import { PostCreateComponent } from './posts/post-create/post-create.component';
import { HomeComponent } from './home/home.component';
import { HomeListComponent } from './home/home-list/home-list.component';
import { HomeViewComponent } from './home/home-view/home-view.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProductComponent } from './product/product.component';
import { ProductAComponent } from './product/product-productA/product-productA.component';
import { ProductBComponent } from './product/product-productB/product-productB.component';
import { ProductCComponent } from './product/product-productC/product-productC.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PostListComponent } from './posts/post-list/post-list.component';
import { AppRoutingModule } from './app-routing.module';
//import { PostsService } from './posts/posts.service';

import { InvoiceService } from "../app/invoice/invoice.service"; 
import { InvoiceSuccessComponent } from "./invoice/invoice-success/invoice-success.component";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    PostCreateComponent,
    HeaderComponent,
    FooterComponent,
    PostListComponent,
    HomeViewComponent,
    HomeListComponent,
    HomeComponent,
    ProductComponent,
    ProductAComponent,
    ProductBComponent,
    ProductCComponent,
    InvoiceComponent,
    InvoiceSuccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    HttpClientModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    QRCodeModule
  ],
  providers: [InvoiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
