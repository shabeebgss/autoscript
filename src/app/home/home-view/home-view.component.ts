import { Component, OnInit } from "@angular/core";
import { Lproduct } from '../home.model';
import { HomeService } from '../home.service';
import { ProductService } from '../../product/product.service';
import { InvoiceService } from '../../invoice/invoice.service';
import { ActivatedRoute, ParamMap  } from '@angular/router';

@Component ({
    selector: 'app-home-view',
    templateUrl: './home-view.component.html',
    styleUrls: ['./home-view.component.css']
})

export class HomeViewComponent implements OnInit {
    private id: string;
    product: Lproduct[] = [];
    isLoading = false;
    serviceName: string;
    icon:string;
    cost:number;
    frequency:string;
    slug_name:string;
    payment_day:number;
    payment_month:number;
    invoiceType:string = null;
    selectedDate: number;
    iPayCost: number;
    sheduleType: number = 2;
    newDay:string;
    payment_date:string;
    selected:number;
    default_day:string;
    productACEnable:boolean = true;
    productBEnable:boolean = false;
    productAEnable:boolean = false;
    public myAngularxQrCode: string = null;
    constructor(public homeService: HomeService, public productService: ProductService, public invoiceService: InvoiceService, public route: ActivatedRoute){
        
    }

       
    ngOnInit() {
        this.route.paramMap.subscribe((paramMap: ParamMap) => {
             if(paramMap.has('id')) {
                 this.isLoading = true;
                 this.id = paramMap.get('id');
                 this.homeService.getProduct(this.id).subscribe(postData => {
                     //console.log(postData);
                     this.isLoading = false;
                     this.serviceName =  postData.name;
                     this.icon = postData.icon;
                     this.cost = postData.cost;
                     this.frequency = postData.frequency;
                     this.slug_name = postData.slug_name;

                     


                     /* set default date */
        const currDate = new Date();
        
       
        if ( currDate.getDate() > 28 ) {
            this.selected = 1;
        }
        else {
            if( this.slug_name == 'A') {
                this.selected = currDate.getDate() + 2;
                this.productAEnable = true;
            }
            else if ( this.slug_name == 'B') {
                this.selected = currDate.getDate();
                this.productACEnable = false;
                this.productBEnable = true;
            }
           else {
                this.selected = currDate.getDate() + 1;
            }  
        }


       //selected date
       const dd = this.selected;
       this.selectedDate = this.selected;
      
       //current date
       var today = new Date();
       var today_dd = today.getDate();
       var today_mm = today.getMonth()+1; 
       var today_yyyy = today.getFullYear();
       var new_today = today_yyyy+'-'+today_mm+'-'+today_dd;

       if( dd >= today_dd) {
        if( this.slug_name == 'A') {
            var mm = currDate.getMonth()+1; 
        }
        else if ( this.slug_name == 'B') {
            var mm = currDate.getMonth()+1; 
        }
       else {
        var mm = currDate.getMonth()+1; 
        } 
          
       }  else {
           var mm = currDate.getMonth()+2; 
       }

       if( dd == today_dd) {
        this.invoiceType = 'regular';
       } else {
        this.invoiceType = 'immediate';
       }

       this.payment_day = dd;
       this.payment_month = mm;
       this.payment_date = today_yyyy+'-'+this.payment_month+'-0'+this.payment_day;
      
       const yyyy = currDate.getFullYear();
       const endDate = yyyy+'-'+mm+'-'+dd;
       
       /*compare the dates*/
       var d11 = new Date(endDate);
       var d22 = new Date(new_today);
       var timeDiff = d11.getTime() - d22.getTime();
       var days = timeDiff / (1000 * 3600 * 24);
       
       //var per_remain_day = (days/30)*100;
      
           //this.iPayCost = this.cost / per_remain_day;
           if( this.slug_name == 'A') {
                this.iPayCost = this.cost / 30 * days;
            }
            else if ( this.slug_name == 'B') {
                this.iPayCost = null;
            }
            else {
                this.iPayCost = null;
            }  

                 });
             }
        });


        this.route.paramMap.subscribe((paramMap: ParamMap) => {
             
             
             
        });

        
    }

    dates = [
        {value: 1, viewValue: '01'},
        {value: 2, viewValue: '02'},
        {value: 3, viewValue: '03'},
        {value: 4, viewValue: '04'},
        {value: 5, viewValue: '05'},
        {value: 6, viewValue: '06'},
        {value: 7, viewValue: '07'},
        {value: 8, viewValue: '08'},
        {value: 9, viewValue: '09'},
        {value: 10, viewValue: '10'},
        {value: 11, viewValue: '11'},
        {value: 12, viewValue: '12'},
        {value: 13, viewValue: '13'},
        {value: 14, viewValue: '14'},
        {value: 15, viewValue: '15'},
        {value: 16, viewValue: '16'},
        {value: 17, viewValue: '17'},
        {value: 18, viewValue: '18'},
        {value: 19, viewValue: '19'},
        {value: 20, viewValue: '20'},
        {value: 21, viewValue: '21'},
        {value: 22, viewValue: '22'},
        {value: 23, viewValue: '23'},
        {value: 24, viewValue: '24'},
        {value: 25, viewValue: '25'},
        {value: 26, viewValue: '26'},
        {value: 27, viewValue: '27'},
        {value: 28, viewValue: '28'}
      ];
      
    
      
      selectChangeHandler(SelDate){
        this.selectedDate = SelDate;
        //this.iPayCost = +this.cost/+SelDate;  

        
        //selected date
        const currDate = new Date();
        const dd = this.selectedDate;
            
        //current date
        var today = new Date();
        var today_dd = today.getDate();
        var today_mm = today.getMonth()+1; 
        var today_yyyy = today.getFullYear();
        var new_today = today_yyyy+'-'+today_mm+'-'+today_dd;

        if( dd >= today_dd) {
            if( this.slug_name == 'A') {
                var mm = currDate.getMonth()+1; 
            }
            else if ( this.slug_name == 'B') {
                var mm = currDate.getMonth()+1; 
            }
           else {
            var mm = currDate.getMonth()+1; 
            } 
              
           }  else {
               var mm = currDate.getMonth()+2; 
           }
        this.payment_day = dd;
        this.payment_month = mm;
        const yyyy = currDate.getFullYear();
        const endDate = yyyy+'-'+mm+'-'+dd;

        /*compare the dates*/
        var d11 = new Date(endDate);
        var d22 = new Date(new_today);
        var timeDiff = d11.getTime() - d22.getTime();
        var days = timeDiff / (1000 * 3600 * 24);
            
        var per_remain_day = (days/30)*100;
           
        //this.iPayCost = this.cost / per_remain_day;
        if( this.slug_name == 'A') {
            this.iPayCost = this.cost / 30 * days;
        }
        else if ( this.slug_name == 'B') {
            this.iPayCost = null;
        }
       else {
        this.iPayCost = null;
        }  
        
        this.payment_date = today_yyyy+'-'+this.payment_month+'-'+this.payment_day;

    }
   
    onSavePost() {
        
        this.productService.addShedule(this.iPayCost,
                                       this.slug_name,
                                       this.cost,
                                        2,
                                        this.payment_day,
                                        this.payment_month,
                                        this.payment_date,
                                        this.sheduleType,
                                        this.invoiceType
                                     );
    } 
}