import { Component} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {
  productA = {
    name: "Blast Off",
    features: [{
        1:"hodl rocket ship",
        2:"x1000 cryptocoins",
        3:"lightning drivemk2"
    }],
    type:"Monthly Price",
    btc:"0.002 BTC"
  }

  productB = {
    name: "WAR EFFORT DONATION",
    features: [{
        1:"DONAT TO THE WAR EFFORT ON A REGULAR BASIS AND GET ACCESS TO EXCLUSIVE CONTENT"
    }],
    type:"Monthly Price",
    btc:"0.003 BTC"
  }

  productC = {
    name: "EXPLORER PACK",
    features: [{
        1:"MINING ON MARS ROVER",
        2:"x3000 cryptocoins",
        3:"alien ios repelent"
    }],
    type:"Monthly Price",
    btc: "0.005 BTC"
  }

}