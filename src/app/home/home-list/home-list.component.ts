import { Component, OnInit } from "@angular/core";
import { PageEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { Lproduct } from '../home.model';
import { HomeService } from '../home.service';
@Component({
    selector: 'app-home-list',
    templateUrl: './home-list.component.html',
    styleUrls: ['./home-list.component.css']
})

export class HomeListComponent implements OnInit{

   lProduct: Lproduct[] = [];
   isLoading = false;
   totalPosts = 0;
   postsPerPage = 3;
   currentPage = 1;
   pageSizeOptions = [1, 3, 5, 10];
   posts: Lproduct[] = [];
   slug_name:string;
   private postsSub: Subscription;

   constructor(public homeService: HomeService) {}

   ngOnInit() {
       this.isLoading = true;
       this.homeService.getProducts();
       this.postsSub = this.homeService.getPostUpdateListener()
        .subscribe((postData: {posts: Lproduct[], postCount: number}) => {
            this.posts = postData.posts;
            this.isLoading = false;
       });
   }


   onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;
    this.homeService.getProducts();
}
}