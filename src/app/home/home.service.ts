import { Lproduct } from './home.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Constants } from '../constants';



@Injectable({providedIn: 'root'})
export class HomeService {
    private posts: Lproduct[] = [];
    private postsUpdated = new Subject<{posts: Lproduct[], postCount: number}>();
    uri:string;
    

    constructor(private http: HttpClient, private router: Router) {
        this.uri = Constants.API_ENDPOINT;
    }

    getProducts(){
        //const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
        this.http
        .get<{message: string, posts: any, maxPosts: number }>(
            this.uri + 'product/'
            )
        .pipe(map((postData) => {
            console.log(postData);
            return { posts: postData.posts.map(post => {
                return {
                    id: post._id,
                    name: post.name,
                    cost: post.cost,
                    icon: post.icon,
                    slug_name: post.slug_name
                   
                };
            }), maxPosts: postData.maxPosts};
        }))
        .subscribe(transformedPostData => {
            this.posts = transformedPostData.posts;
            this.postsUpdated.next({ 
                posts: [...this.posts],
                postCount: transformedPostData.maxPosts 
            });
        });
    }

    getPostUpdateListener() {
        return this.postsUpdated.asObservable();
    }


    getProduct(id: string) {
        return this.http.get<{_id: string, name: string, cost: number, icon: string, frequency: string, slug_name: string}>(
            this.uri + 'product/' + id
            );
    }

   


    
}