
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from '../constants';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})

export class InvoiceService {

    uri : string;
    
    constructor(private http: HttpClient, private router: Router) {
        this.uri = Constants.API_ENDPOINT;
    }

    //uri = 'http://localhost:3000/api/';
    //uri = 'http://51.158.68.20:5000/api/';
    

    getShedule(id: string) {
        return this.http.get<{_id: string, qr_code: string}>(
            this.uri + 'invoice/' + id
            );
    }

    checkPayStatus(status: string) {
        return this.http.get<{_id: string, payment_status: number}>(
            this.uri + 'invoice/' + status
            );
    }

}