import { Component, OnInit } from "@angular/core";
import { InvoiceService } from './invoice.service';
import { Invoice } from './invoice.model';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap  } from '@angular/router';

@Component ({
    selector: 'app-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.css']
})

export class InvoiceComponent implements OnInit {
    private id: string;
    private status: string;
    invoices: Invoice[] = [];
    isLoading = false;
    payStatusFlag = false;
    payStatus:number;
    
    public myAngularxQrCode: string = null;
    constructor(public invoiceService: InvoiceService, public route: ActivatedRoute, private router: Router){
        
    }
       
    ngOnInit() {
        this.route.paramMap.subscribe((paramMap: ParamMap) => {
             if(paramMap.has('id')) {
                 this.isLoading = true;
                 this.id = paramMap.get('id');
                 this.status = paramMap.get('id');
                 this.invoiceService.getShedule(this.id).subscribe(postData => {
                    this.isLoading = false;
                    this.myAngularxQrCode =  postData.qr_code;
                    //console.log(this.myAngularxQrCode);
                    this.invoiceService.checkPayStatus(this.status).subscribe(payStatusInfo => {
                        this.isLoading = false;
                        this.payStatus =  payStatusInfo.payment_status;
                        console.log("Payment status= "+this.payStatus);
                        if( this.payStatus == 2 ) {
                            this.router.navigate(["/success"]);
                        }
                    });
                 });
             }
        });
    }
}