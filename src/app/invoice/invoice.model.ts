export interface Invoice {
    paymentDay: number,
    paymentAmount: number,
    immediatePaymentAmount:number,
    schedule_type:number,
    frequency:number,
    payment_month:number,
    payment_date:string,
    invoiceType:string,
    qr_code:string,
    shedule_parent_id:string,
    shedule_id:string
    
}